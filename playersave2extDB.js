var mysql = require("mysql"),
  util = require("util"),
  async = require("async"),
  fs = require("fs"),
  mysql_config = require("./config.json")["mysql_config"];


var connection = mysql.createConnection(mysql_config);

connection.connect();


var insertPlayerSave = function (uid, info, next) {
  var query = "insert into playersave (" +
    "PlayerUID, MapID, CreationDate, LastModified, Position, Direction, Damage, HitPoints, " +
    "Hunger, Thirst, Money, CurrentWeapon, Stance, Headgear, Goggles, Uniform, Vest, Backpack, " +
    "UniformWeapons, UniformItems, UniformMagazines, " +
    "VestWeapons, VestItems, VestMagazines, " +
    "BackpackWeapons, BackpackItems, BackpackMagazines, " +
    "PrimaryWeapon, SecondaryWeapon, HandgunWeapon, " +
    "PrimaryWeaponItems, SecondaryWeaponItems, HandgunItems, " +
    "AssignedItems, LoadedMagazines, WastelandItems " +
    ") values ( " +
    "?, ?, now(), now(), ?, ?, ?, ?, " +
    "?, ?, ?, ?, ?, ?, ?, ?, ?, ?, " +
    "?, ?, ?, " +
    "?, ?, ?, " +
    "?, ?, ?, " +
    "?, ?, ?, " +
    "?, ?, ?, " +
    "?, ?, ? " +
    ");"


  var data = [
    uid,
    2, //Map ID
    JSON.stringify(info["Position"]),
    JSON.stringify(info["Direction"]),
    info["Damage"],
    JSON.stringify(info["HitPoints"]),
    info["Hunger"],
    info["Thirst"],
    info["Money"],
    info["CurrentWeapon"],
    info["Stance"],
    info["Headgear"],
    info["Goggles"],
    info["Uniform"],
    info["Vest"],
    info["Backpack"],
    JSON.stringify(info["UniformWeapons"]),
    JSON.stringify(info["UniformItems"]),
    JSON.stringify(info["UniformMagazines"]),
    JSON.stringify(info["VestWeapons"]),
    JSON.stringify(info["VestItems"]),
    JSON.stringify(info["VestMagazines"]),
    JSON.stringify(info["BackpackWeapons"]),
    JSON.stringify(info["BackpackItems"]),
    JSON.stringify(info["BackpackMagazines"]),
    info["PrimaryWeapon"],
    info["SecondaryWeapon"],
    info["HandgunWeapon"],
    JSON.stringify(info["PrimaryWeaponItems"]),
    JSON.stringify(info["SecondaryWeaponItems"]),
    JSON.stringify(info["HandgunItems"]),
    JSON.stringify(info["AssignedItems"]),
    JSON.stringify(info["LoadedMagazines"]),
    JSON.stringify(info["WastelandItems"])
  ];

  var sql = mysql.format(query, data);

  console.log(sql);

  connection.query(sql, function (err, result) {
    console.log(sql + "\n");
    if (err) {
      return next(err);
    }

    console.log(result.insertId);
    return next(null);
  });


};

var fs = require('fs');

var files = fs.readdirSync('./data/');


async.each(files,
  function (file, next) {

    var data = require("./data/" + file);
    if (!data["PlayerInfo"]) {
      return next(null);
    }
    if (!data["PlayerSave"]) {
      return next(null);
    }
    var uid = data["PlayerInfo"]["UID"];
    insertPlayerSave(uid, data["PlayerSave"], next);
  },
  function (err) {
    if (err) {
      console.log(err);
    }
    connection.end();
    console.log("Done");
  }
);


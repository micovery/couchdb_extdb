var mysql = require("mysql"),
  async = require("async"),
  mysql_config = require("./config.json")["mysql_config"];


var connection = mysql.createConnection(mysql_config);

connection.connect();

var queries = [
  "drop table if exists playerinfo3",
  "create table playerinfo3 like playerinfo",

  "drop table if exists playerstats3",
  "create table playerstats3 like playerstats",

  "drop table if exists playersave3",
  "create table playersave3 like playersave",

  "drop table if exists serverobjects3",
  "create table serverobjects3 like serverobjects",

  "drop table if exists servervehicles3",
  "create table servervehicles3 like servervehicles"

];

async.eachSeries(queries,
  function(query, next) {
    console.log(query);
    connection.query(query, function (err, result) {
      if (err) {
        next(err);
        return;
      }
      console.log(result);
      next(err);
    });
  },
  function(err) {
    if (err) {
      console.log(err);
    }
    connection.end();
    console.log("Done");
  }
);
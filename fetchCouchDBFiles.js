var util = require("util"),
  cradle = require("cradle");
  fs = require("fs"),
  async = require("async"),
  config = require("./config.json");

var con = new (cradle.Connection)(config["couchDB_url"]);

var db = con.database('a3w');

var allIds = [];
db.all(function (err, res) {
  if (err) {
    console.log('Error: %s', err)
  } else {

    async.eachSeries(res,
      function (ddoc, next) {
        allIds.push(ddoc["id"]);
        setImmediate( function() { next(null); });
      },
      function (err) {
        if (err) {
          console.log(err);
        }

        async.each(allIds,
          function (id, next) {
            db.get(id,
              function (err, sdoc) {
                if (err) {
                  return setImmediate(function() {next(err);});
                }
                console.log(id);
                fs.writeFile("./data/" + id + ".json", JSON.stringify(sdoc, null, 2));
                return setImmediate(function() {next(null);});
              }
            );
          },
          function (err) {
            if (err) {
              console.log(err);
            }
            console.log("Done");
          }
        );
      }
    );
  }
});

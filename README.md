```

Structure of the scripts for migration.

couchdb_ext/config.json: Contains the connection information for both CouchDB, and MySQL
couchdb_ext/fetchCouchDBFiles.js: This script connects to couchdb and downlods all the documents fromm the database into the "data" directory

Once all documents have been fetched to the local data directory, you'll see there are three kinds of documents

couchdb_ext/data/A3W_<PLAYER-GUID>.json: These documents contain the players in-game information 
couchdb_ext/data/A3W_Vehicles.json: Document containing information about all vehicles in the map
couchdb_ext/data/A3W_Objects.json: Document containing information about all objects in the map

Note that the prefix "A3W_" might change, depending of the server's prefix, it could be "A3W2_", "Foo_", etc.

Normally, the vehicles and objects files are server specific, but the player files are shared between servers.

The way to do the migration is to convert each of these files to the appropriate insert SQL statements for extDb.

In order to accomplish this, there are multiple scripts:

You can run the scripts simply by running:  "node <scriptname>".

(Before you run the scripts, do "npm install" in the main directory to install the dependencies)

Note that you may have to edit the scripts to generate the correct SQL statement ... e.g. modifying the target table name,  number of columns etc. 

#1
couchdb_extdb/objects2extDB.js: 

This script reads the A3W_Objects.json and traverses it in order to generate an insert SQL statement for the "serverobjects" extDB table for each object.

Once the SQL statement is generated, it goes ahead and tries to execute the query against MySQL.

The JavaScript function that does the generation and execution is "insertObject".


#2
couchdb_extdb/vehicles2extDB.js:

This script reads the A3W_Vehicles.json and traverses it in order to generate an insert SQL statement for the "servervehicles" extDB table for each vehicle.

Once the SQL statement is generated, it goes ahead and tries to execute the query against MySQL.

The JavaScript function that does the generation and execution is "insertVehicle".

To execute this script, simply run: "node vehicles2extDB.js"

#3
couchdb_extdb/playerinfo2extDB.js
This script reads all the files from the data directory, and iterates through them one by one. If after parsing each file, it sses that it contains a "PlayerInfo" section, then
it assumes that it is a player data file (A3W_76561197960269258.json) and tries to generate and execute an insert SQL statement for the "playerinfo" extDB table.

The Javascript function that does the generation and execution of the statement is "insertPlayerInfo".


#4
couchdb_extdb/playersave2extDB.js

This script reads all the files from the data directory, and iterates one by one. If after parsing the each file, it see that it contains both "PlayerInfo" and "PlayerSave" sections,  then
it assumes that it is a player data file, and tries to generate and execute an insert SQL statement for the "playersave" extDB table.

The Javascript function that does the generation and execution of the statement is "insertPlayerSave".


#5
couchdb_extdb/playerstats2extDB.js

This script reads all the files from the data directory, and iterates one by one. If after parsing each file, it sees that it contains both "PlayerInfo" and "PlayerScore" sections, then
it assumes that it is a player data file, and tries to generate and execute an insert SQL statement for the "playerstats" extDB table.

The Javascript function that does the generation and execution of the statement is "insertPlayerStats".


```










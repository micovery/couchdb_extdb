var mysql = require("mysql"),
  util = require("util"),
  async = require("async"),
  mysql_config = require("./config.json")["mysql_config"];


var connection = mysql.createConnection(mysql_config);

connection.connect();

var objects = require("./data/A3W2_Vehicles.json");
var keys = Object.keys(objects);

var insertVehicle = function(key, obj, callback) {

    var query = "insert into servervehicles ( " +
      "ServerID, MapID, CreationDate, LastUsed, " +
      "Class, Position, Direction, Velocity, Flying, Damage, Fuel, HitPoints, Variables, Textures," +
      "Weapons, Magazines, Items, Backpacks, TurretMagazines, TurretMagazines2, TurretMagazines3, " +
      "AmmoCargo, Fuelcargo, RepairCargo) " +
      "values(?,?,now(),now(),?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);";

    if (!obj["Class"]) {
      return callback(null);
    }


    console.log("Processing " + key);

    var data = [
      1, //Server ID
      2, //Map ID
      obj["Class"],
      JSON.stringify(obj["Position"]),
      JSON.stringify(obj["Direction"]),
      JSON.stringify([0, 0, 0]), //velocity
      0, //flying
      obj["Damage"],
      obj["Fuel"],
      JSON.stringify(obj["Hitpoints"]),
      JSON.stringify(obj["Variables"]),
      JSON.stringify([]),
      JSON.stringify(obj["Weapons"]),
      JSON.stringify(obj["Magazines"]),
      JSON.stringify(obj["Items"]),
      JSON.stringify(obj["Backpacks"]),
      JSON.stringify(obj["TurretMagazines"]),
      JSON.stringify(obj["TurretMagazines2"]),
      JSON.stringify(obj["TurretMagazines3"]),
      obj["AmmoCargo"],
      obj["FuelCargo"],
      obj["RepairCargo"]
    ];

    var sql = mysql.format(query, data);

    console.log(sql + "\n");

    connection.query(sql, function (err, result) {
      if (err) {
        return callback(err);
      }
      console.log(result.insertId);
      return callback(err);
    });
};

async.eachSeries(
  keys,
  function (key, next) {
    var obj = objects[key];
    insertVehicle(key, obj, next);
  },
  function (err) {
    if (err) {
      console.log(err);
    }
    console.log("Done");
    connection.end();
  }
);




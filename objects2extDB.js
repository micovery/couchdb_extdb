var mysql = require("mysql"),
  util = require("util"),
  async = require("async"),
  mysql_config = require("./config.json")["mysql_config"];


var connection = mysql.createConnection(mysql_config);

connection.connect();


var objects = require("./data/A3W_Objects.json");
var keys = Object.keys(objects);


var insertObject = function(key, obj, callback) {

    var query = "insert into serverobjects ( " +
      "ServerID, MapID, CreationDate, LastInteraction, " +
      "OwnerUID, Class, Position, Direction, Locked, Deployable, Damage, AllowDamage, Variables, Weapons, Magazines,  " +
      "Items, Backpacks, TurretMagazines, AmmoCargo, Fuelcargo, RepairCargo) " +
      "values(?,?,now(),now(),?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);";

    if (!obj["Class"]) {
      return callback(null);
    }
    
    console.log("Processing " + key);

    //look for owner UI in variables
    var owneruID = "";
    var vars = obj["Variables"];
    if (util.isArray(vars)) {
      for (var i = 0; i < vars.length; i++) {
        var key_value_pair = vars[i];
        var key = key_value_pair[0];
	      var value = key_value_pair[1];
        if (key == "ownerUID") {
          ownerUID = value;
	        break;
	      }
      };
    }

    var OR = function (value, other) {
      if (value == null || value == undefined) {
        return other;
      }

      return value;
    };

    var data = [
      1, //Server ID
      2, //Map ID
      ownerUID,
      obj["Class"],
      JSON.stringify(obj["Position"]),
      JSON.stringify(obj["Direction"]),
      1, //Locked,
      0, //Deployable
      obj["Damage"],
      obj["AllowDamage"],
      JSON.stringify(obj["Variables"]),
      JSON.stringify(obj["Weapons"]),
      JSON.stringify(obj["Magazines"]),
      JSON.stringify(obj["Items"]),
      JSON.stringify(obj["Backpacks"]),
      JSON.stringify(obj["TurretMagazines"]),
      OR(obj["AmmoCargo"], 1),
      OR(obj["FuelCargo"], 1),
      OR(obj["RepairCargo"], 1),
    ];

    var sql = mysql.format(query, data);

    console.log(sql);

    connection.query(sql, function (err, result) {
      if (err) {
        return callback(err);
      }
      console.log(result.insertId);
      return callback(err);
    });
};

async.eachSeries(
  keys,
  function (key, next) {
    var obj = objects[key];
    insertObject(key, obj, next);
  },
  function (err) {
    if (err) {
      console.log(err);
    }
    console.log("Done");
    connection.end();
  }
);




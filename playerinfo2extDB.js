var mysql = require("mysql"),
  util = require("util"),
  async = require("async"),
  fs = require("fs"),
  bigInt = require("big-integer"),
  CryptoJS = require("crypto-js"),
  mysql_config = require("./config.json")["mysql_config"];


var connection = mysql.createConnection(mysql_config);

connection.connect();

var uid2guid = function (uid) {
  if (!uid) {
    return;
  }

  var steamId = bigInt(uid);

  var parts = [0x42, 0x45, 0, 0, 0, 0, 0, 0, 0, 0];

  for (var i = 2; i < 10; i++) {
    var res = steamId.divmod(256);
    steamId = res.quotient;
    parts[i] = res.remainder.toJSNumber();
  }

  var wordArray = CryptoJS.lib.WordArray.create(new Uint8Array(parts));
  var hash = CryptoJS.MD5(wordArray);
  return hash.toString();
};


var insertPlayerInfo = function (uid, guid, info, next) {
  var query = "insert into playerinfo (UID, CreationDate, Name, LastSide, BankMoney, BattlEyeGUID) values (?,now(), ?, ?, ? ,?);";

  var data = [
    uid,
    info["Name"],
    info["LastPlayerSide"],
    info["BankMoney"],
    guid
  ];

  var sql = mysql.format(query, data);
  
  console.log(sql);

  connection.query(sql, function (err, result) {
    if (err) {
      return next(err);
    }
    console.log(sql + "\n");

    console.log(result.insertId);
    return next(null);
  });
};

var fs = require('fs');

var files = fs.readdirSync('./data/');


async.each(files,
  function (file, next) {

    var data = require("./data/" + file);
    if (!data["PlayerInfo"]) {
      return next(null);
    }
    var uid = data["PlayerInfo"]["UID"];
    var guid = uid2guid(data["PlayerInfo"]["UID"]);
    insertPlayerInfo(uid, guid, data["PlayerInfo"], next);
  },
  function (err) {
    if (err) {
      console.log(err);
    }
    connection.end();
    console.log("Done");
  }
);


var mysql = require("mysql"),
  util = require("util"),
  async = require("async"),
  fs = require("fs"),
  mysql_config = require("./config.json")["mysql_config"];


var connection = mysql.createConnection(mysql_config);

connection.connect();


var insertPlayerStats = function (uid, info, next) {
  var query = "insert into playerstats (" +
    "PlayerUID, LastModified," +
    "PlayerKills, AIKills, TeamKills," +
    "DeathCount, ReviveCount, CaptureCount" +
    ") values ( " +
    "?, now()," +
    "?, ?, ?," +
    "?, ?, ? " +
    ");";

  var data = [
    uid,
    info["playerKills"],
    info["aiKills"],
    0, //team-kills
    info["deathCount"],
    info["reviveCount"],
    info["captureCount"]
  ];

  var sql = mysql.format(query, data);
  
  console.log(sql);

  connection.query(sql, function (err, result) {
    console.log(sql + "\n");
    if (err) {
      return next(err);
    }

    console.log(result.insertId);
    return next(null);
  });


};

var fs = require('fs');

var files = fs.readdirSync('./data/');


async.each(files,
  function (file, next) {

    var data = require("./data/" + file);
    if (!data["PlayerInfo"]) {
      return next(null);
    }
    if (!data["PlayerScore"]) {
      return next(null);
    }
    var uid = data["PlayerInfo"]["UID"];
    insertPlayerStats(uid, data["PlayerScore"], next);
  },
  function (err) {
    if (err) {
      console.log(err);
    }
    connection.end();
    console.log("Done");
  }
);

